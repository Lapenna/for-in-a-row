
#include <openssl/err.h>
#include "ClientOp.h"
#include <iostream>
#include <openssl/evp.h>
#include "../common/operations/Operations.h"

#define PORT '9999'
#define SERVER "127.0.0.1"
#define BUFFER 1024

using namespace std;

int main(int argc, char *argv[]) {
    Session *pSession = new Session();
    Client *pClient = new Client();

    OPENSSL_init();
    OpenSSL_add_all_algorithms();

    if (argc < 2) {
        cerr << "Error: Parameters are missing" << "\n";
        return 0;
    }

    if (pClient->Init(argv[1]) < 0) {
        cerr << "Error: impossible init client" << "\n";
        return 0;
    }

    pSession->session_id = OpenSession(SERVER, PORT);
    if (pSession->session_id < 0) {
        cerr << "Error to open Session" << "\n";
        return 0;
    }

    if (Authenticate(pSession, pClient) < 0) {
        cerr << "Error: autenticator error" << "\n";
        return 0;
    }
    if (SessionKeyNegotation(pSession, pClient->pkey) < 0) {
        cerr << "Error: Negotation error" << "\n";
        return 0;
    }
    Application(pClient, pSession);

    EVP_cleanup();

    return 0;
}
