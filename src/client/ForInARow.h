//
// Created by Federico Cosimo Lapenna on 02/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_FORINAROW_H
#define CLIENTPROJECTSECURITY_FORINAROW_H

#include "../common/model/Session.h"
#include "model/Client.h"

int RunGame(Session *pSession, Client *pClient, bool first_turn);

#endif //CLIENTPROJECTSECURITY_FORINAROW_H
