//
// Created by Federico Cosimo Lapenna on 09/05/2020.
//
#include <openssl/ossl_typ.h>
#include <cstring>
#include <sys/socket.h>
#include <unistd.h>
#include <cstdio>
#include <arpa/inet.h>
#include <iostream>
#include <poll.h>
#include <list>
#include <openssl/rand.h>
#include "ClientOp.h"
#include "../common/operations/Operations.h"
#include "model/Client.h"
#include "ForInARow.h"
#include "../common/operations/KeyExchange.h"

#define SA struct sockaddr
#define BUFFER 1024

using namespace std;

//////////////////////////AUTENTICATION PROTOCOL//////////////////////////////////

int SendValidSignAndNonces(EVP_PKEY *pkey, Session *pSession) {
    vector<unsigned char> message;
    Payload payload;

    payload.setType(CLIENT_VERIFY_TYPE, SEND_MSG);
    payload.setFriendNonce(pSession->friend_nonce, SEND_MSG);
    payload.setMyNonce(pSession->my_nonce, SEND_MSG);

    message = payload.getPayload();

    if (SendMessage(pSession->session_id, message, message.size(), pkey, CHECK_SIGN) < 0) {
        cerr << "Error:To send client_hello message " << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int RecServerHello(Session *pSession) {
    Payload payload;

    Message message = ReceiveMessage(pSession, NO_CRIPT_MESSAGE, CHECK_SIGN);
    if (message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    if (payload.setPeyload(message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(payload.type, SERVER_HELLO_TYPE) > 0) {
        cout << "it's was recived an server_hello message" << "\n";

        if (payload.my_nonce.empty()) {
            cerr << "Impossible extract Attribute: " << RESPONSE_NONCE_ATTRIBUTE << "\n";
            return FAIL;
        }
        //CHECK NONCE
        if (payload.my_nonce.empty()) {
            cerr << "Impossible extract Attribute: " << RESPONSE_NONCE_ATTRIBUTE << "\n";
            return FAIL;
        }

        if (VerifyNonce(payload.my_nonce, MY_NONCE, pSession) < 0) {
            cerr << "Error to autenticate, nonce is different" << "\n";
            return FAIL;
        }

        if (SetSessionNonce(&payload, pSession) < 0) { //SET SESSION NONCE
            cerr << "Error to set session nonce" << "\n";
            return FAIL;
        }
    } else {
        cerr << "ERROR server hello message" << "\n";
        return FAIL;
    }
    return SUCCEED;
}

int SendClientHello(Client *pClient, Session *pSession) {
    int nonce;
    vector<unsigned char> message;
    Payload payload;

    nonce = GenNonce(); //GENERATE FRESH ELEMENT
    if (nonce == 0) {
        cerr << "Invalid nonce" << "\n";
        return FAIL;
    }
    pSession->my_nonce = nonce;

    payload.setType(CLIENT_HELLO_TYPE, SEND_MSG);

    if (payload.setUserName(pClient->user_name, pClient->user_name_len, SEND_MSG) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }
    payload.setMyNonce(nonce, SEND_MSG);

    message = payload.getPayload();
    if (message.empty()) {
        cerr << "message is empty" << '\n';
        return FAIL;
    }

    if (SendMessage(pSession->session_id, message, message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error:To send client_hello message " << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int Authenticate(Session *pSession, Client *pClient) {
    cout << "Start autentication protocol" << "\n";
    if (pSession->session_id < 0) {
        cerr << "Error: error to start connection with server" << "\n";
        goto err;
    }

    if (SendClientHello(pClient, pSession) < 0) {
        cerr << "Error: error to send clienthello message to server " << "\n";
        goto err;
    }

    if (WaitResponse(pSession, NO_CRIPT_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error to start connection with server, prob user name is wrong" << "\n";
        return FAIL;
    }

    if (ReceiveCertificate(pSession, pClient) < 0) {
        char *err = "Error: Error to get Server Certificate ";
        cerr << "Error: Error to get Server Certificate " << "\n";
        if (SendError(pSession, pClient->pkey, err, NO_CRIPT_MESSAGE, NO_CHECK_SIGN) < 0) {
            cerr << "Error to send error message to server " << "\n";
            goto err;
        }
        goto err;
    }
    if (SendOk(pSession, pClient->pkey, NO_CRIPT_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send ok message to server " << "\n";
        return SUCCEED;
    }

    cout << "Wait Server Hello" << "\n";
    if (RecServerHello(pSession) < 0) {
        cerr << "Error: error to elaborate Server_hello message from server " << "\n";
        goto err;
    }
    cout << "Send prove" << "\n";
    if (SendValidSignAndNonces(pClient->pkey, pSession) < 0) {
        std::cerr << "ERROR to send the valid sign on server" << "\n";
        goto err;
    }
    if (WaitResponse(pSession, NO_CRIPT_MESSAGE, CHECK_SIGN) < 0) {
        goto err;
    }

    cout << "Autenticate OK" << "\n";

    return SUCCEED;

    err:
    return FAIL;
}

//////////////////////////APPLICATION//////////////////////////////////

int SendGameChose(int column, Session *pSession, Client *pClient) {
    vector<unsigned char> message;
    Payload payload;

    payload.setType(GAME_TYPE, SEND_MSG);
    payload.setGameMove(column);

    message = payload.getPayload();
    message = EncryptMessage(message.data(), message.size(), pSession->session_key, pSession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        goto err;
    }

    if (SendMessage(pSession->session_id, message, message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send message" << "\n";
        goto err;
    }
    if (WaitResponse(pSession, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error to set column, probably opponent is disconect" << "\n";
        return FAIL;
    }

    return SUCCEED;

    err:
    char *err = "error during the column chose or when try to send message";
    if (SendError(pSession, pClient->pkey, err, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "error when try to send error";
    }
    return FAIL;
}

int ReceiveGameChoise(Session *pSession) {
    vector<unsigned char> type;
    vector<unsigned char> object;
    Message rec_message;
    Payload payload;
    int column = 0;

    rec_message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN);
    if (rec_message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }
    rec_message = DecryptMessage(rec_message, pSession->session_key, pSession);
    if (rec_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    if (payload.setPeyload(rec_message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(payload.type, GAME_TYPE) > 0) {
        if (payload.game_move.empty()) {
            cerr << "column is empty" << '\n';
            return FAIL;
        }

        memcpy(reinterpret_cast<void *>(&column), payload.game_move.data(), sizeof(int));
        if (column < 0) {
            cerr << "column value is invalid" << '\n';
            return FAIL;
        }
    } else {
        cerr << "type message is wrong" << '\n';
        return FAIL;;
    }

    return column;
}

int CloseConnection(int conn, Client *pClient, Session *pSession) {
    vector<unsigned char> message;
    Payload payload;

    payload.setType(CLOSE_CONN_TYPE, SEND_MSG);

    message = payload.getPayload();
    message = EncryptMessage(message.data(), message.size(), pSession->session_key, pSession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(conn, message, message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send message" << "\n";
        return FAIL;
    }
    if (WaitResponse(pSession, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error to close connection" << "critical error, abort" << "\n";
        abort();
    }

    close(conn);
    return SUCCEED;
}

//get list of opponent by the message recived by the server
vector<unsigned char *> GetListByMessage(vector<unsigned char> v) {
    vector<unsigned char *> users;
    vector<unsigned char> temp;
    unsigned char *temp_char;

    for (int i = 0; v[i] != '\0'; i++) {
        if (v[i] == '|') {
            if (!temp.empty()) {
                temp_char = new unsigned char[temp.size()];
                memcpy(temp_char, temp.data(), temp.size());
                users.push_back(temp_char);
                temp.clear();
            }
        } else {
            temp.push_back(v[i]);
        }
    }
    return users;
}

///ASK ACTIVE USER
unsigned char *ShowUsersList(Client *pClient, Session *pSession) {
    vector<unsigned char> send_message;
    Message rec_message;
    vector<unsigned char> type;
    Payload send_payload;

    send_payload.setType(AVAILABLE_USERS_TYPE, SEND_MSG);
    send_message = send_payload.getPayload();
    send_message = EncryptMessage(send_message.data(), send_message.size(), pSession->session_key, pSession);
    if (send_message.empty()) {
        cerr << "Message is empty" << "\n";
        return NULL;
    }

    if (SendMessage(pSession->session_id, send_message, send_message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return NULL;
    }

    rec_message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN);
    if (rec_message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return NULL;
    }
    rec_message = DecryptMessage(rec_message, pSession->session_key, pSession);
    if (rec_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return NULL;
    }

    Payload recv_payload;
    if (recv_payload.setPeyload(rec_message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return NULL;
    }

    if (recv_payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return NULL;
    }

    if (VerifyTypeMessage(recv_payload.type, AVAILABLE_USERS_TYPE) < 0) {
        cerr << "wrong type of message" << "\n";
        return NULL;
    }
    vector<unsigned char *> list_users;
    int i = 0;
    int chose = 0;

    if (!recv_payload.users.empty()) {
        list_users = recv_payload.getUsersList();
    }

    if (list_users.empty()) {
        cout << "No available user" << '\n';
        return NULL;
    }

    cout << "Chose the user: " << '\n'
         << "-1)Chose nobody,close" << '\n';

    for (vector<unsigned char *>::iterator it = list_users.begin(); it != list_users.end(); it++, i++) {
        cout << i << ")" << *it << '\n';
    }
    cout << "who chose :";
    while (!(cin >> chose) || chose < -1 || chose > i - 1) {
        // Explain Error
        cout << "ERROR: You must enter a valid chose";
        // Clear input stream
        cin.clear();
        // Discard previous input
        cin.ignore(132, '\n');

        cout << "Chose the user: " << '\n';
    }
    if (chose == -1) {
        return NULL;
    }

    return list_users[chose];
}

//tells the server that it has to free everything that concerns itself
// for example challenge
//usually it is called at the and of game
int FreeClient(Client *pClient, Session *pSession) {
    vector<unsigned char> send_message;
    Payload payload;

    payload.setType(FREE_CLIENT_TYPE, SEND_MSG);
    send_message = payload.getPayload();

    send_message = EncryptMessage(send_message.data(), send_message.size(), pSession->session_key, pSession);
    if (send_message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, send_message, send_message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int SendRejectMessage(Client *pClient, Session *pSession, unsigned char *adversary) {
    vector<unsigned char> send_message;
    Payload payload;

    payload.setType(REJECT_TYPE, SEND_MSG);
    if (payload.setUserName(adversary, strnlen(reinterpret_cast<const char *>(adversary), USER_NAME_SIZE_MAX),
                            SEND_MSG) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    send_message = payload.getPayload();
    send_message = EncryptMessage(send_message.data(), send_message.size(), pSession->session_key, pSession);
    if (send_message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, send_message, send_message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int SendAcceptMessage(Client *pClient, Session *pSession, unsigned char *adversary) {
    vector<unsigned char> send_message;
    Payload payload;

    payload.setType(ACCEPT_TYPE, SEND_MSG);
    if (payload.setUserName(adversary, strnlen(reinterpret_cast<const char *>(adversary), USER_NAME_SIZE_MAX),
                            SEND_MSG) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    send_message = payload.getPayload();
    send_message = EncryptMessage(send_message.data(), send_message.size(), pSession->session_key, pSession);
    if (send_message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, send_message, send_message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int SeeChallenge(Client *pClient, Session *pSession) {

    vector<unsigned char> send_message;
    Message rec_message;
    vector<unsigned char> adversary;
    string chose;
    Payload payload;

    payload.setType(CHALLENGES_RECEIVED_TYPE, SEND_MSG);
    send_message = payload.getPayload();
    send_message = EncryptMessage(send_message.data(), send_message.size(), pSession->session_key, pSession);
    if (send_message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, send_message, send_message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    rec_message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN);
    if (rec_message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }
    rec_message = DecryptMessage(rec_message, pSession->session_key, pSession);
    if (rec_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }
    Payload recv_payload;

    if (recv_payload.setPeyload(rec_message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (recv_payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(recv_payload.type, CHALLENGES_RECEIVED_TYPE) > 0) {
        adversary = recv_payload.user;
        if (adversary.empty()) {
            return 2;
        }
        adversary.push_back('\0');

        cout << adversary.data() << " has challenged you, do you accept?" << '\n'
             << "y(for yes) or n (for no)" << '\n' << "Chose:";

        string yes("y");
        string no("n");

        do {
            std::getline(std::cin, chose);
            if (!std::cin) {
                std::cerr << "std in error" << '\0';
                return -1;
            }
        } while (chose.compare(yes) != 0 && chose.compare(no) != 0);

        if (chose.compare(no) == 0) {  //REJECT CHALLENGE
            if (SendRejectMessage(pClient, pSession, adversary.data()) < 0) {
                cerr << "Error to send reject message" << '\n';
                return FAIL;
            }
            return 0;
        } else if (chose.compare(yes) == 0) { //ACCEPT CHALLENGE
            if (SendAcceptMessage(pClient, pSession, adversary.data()) < 0) {
                cerr << "Error to send accept message" << '\n';
                return FAIL;
            }
            //delete[] buf;
            return SUCCEED;
        }
    } else {
        cerr << "Message is invalid" << "\n";
        return FAIL;
    }
}

//CLIENT RECIVE PUBKEY AND SEND SESSION KEY
int StartChallenge(Client *pClient, Session *pSession, unsigned char *adversary) {
    vector<unsigned char> send_message;
    Message rec_message;
    Payload payload;

    payload.setType(CHALLENGE_USER_TYPE, SEND_MSG);
    if (payload.setUserName(adversary, strnlen(reinterpret_cast<const char *>(adversary), USER_NAME_SIZE_MAX),
                            SEND_MSG) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    int timeout = 60000;  //60 s
    struct pollfd fds[2];
    // Initialize the fds to 0
    memset(fds, 0, sizeof(fds));

    fds[0].fd = pSession->session_id;
    fds[0].events = POLLIN;

    send_message = payload.getPayload();
    send_message = EncryptMessage(send_message.data(), send_message.size(), pSession->session_key, pSession);
    if (send_message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, send_message, send_message.size(), pClient->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }
    if (WaitResponse(pSession, GCM_MESSAGE, NO_CHECK_SIGN) < 0) { //WAIT SERVER OK
        return FAIL;
    }

    cout << "Wait opponent.... (max 60s)" << '\n';

    //wait response of client 60s
    if (poll(fds, 2, timeout) == 0) {
        cout << "Opponent did not respond to the challenge" << '\n';
        if (FreeClient(pClient, pSession) < 0) {
            cerr << "Error when try to send free client message" << "\n";
            return FAIL;
        }
        return 0;
    }

    Payload recv_payload;

    rec_message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN); //WAIT ADVERSARY RESPONSE
    if (rec_message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }
    rec_message = DecryptMessage(rec_message, pSession->session_key, pSession);
    if (rec_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    if (recv_payload.setPeyload(rec_message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (recv_payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(recv_payload.type, ACCEPT_TYPE) > 0) {
        cout << "Adversary accept the challenge" << '\n';
        return SUCCEED;
    } else if (VerifyTypeMessage(recv_payload.type, REJECT_TYPE) > 0) {
        return 0;
    }

    cerr << "Message is invalid" << "\n";
    return FAIL;
}

int ReceiveAndSetPubKeyOfOpponent(Session *pSession, Session *pOpponentSession) {
    cout << "Wait oppenent pub key..." << '\n';

    Message rec_message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN);
    if (rec_message.ct.empty()) {
        cerr << "Error to OK message, Negotation is failed " << "\n";
        return FAIL;
    }
    rec_message = DecryptMessage(rec_message, pSession->session_key, pSession);
    if (rec_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    vector<unsigned char> pub_key;
    pub_key.insert(pub_key.begin(), rec_message.pt.begin(), rec_message.pt.end());
    pub_key.push_back('\0');
    EVP_PKEY *pKey = getPubKeyEVP_PKEY(pub_key.data());
    if (!pKey) {
        cerr << "Error:Key is not valid, Negotation failed " << "\n";
        return FAIL;
    }

    pOpponentSession->pPub_key = pKey;
    return SUCCEED;
}

int AutenticateOpponent(Session *pSession, Client *pClient) {

    cout << "start apponent autenticate.." << '\n';

    if (SendClientHello(pClient, pSession) < 0) {
        cerr << "Error to send client hello" << "\n";
        return FAIL;
    }

    Payload recv_payload;

    Message recv_message = ReceiveMessage(pSession, NO_CRIPT_MESSAGE, NO_CHECK_SIGN);
    if (recv_message.ct.empty() || recv_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    if (recv_payload.setPeyload(recv_message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (recv_payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(recv_payload.type, CLIENT_HELLO_TYPE) < 0) {
        cerr << "Type message is wrong" << "\n";
        return FAIL;
    }

    if (SetSessionNonce(&recv_payload, pSession) < 0) {
        cerr << "Error:Impossible set friend nonce" << "\n";
        return FAIL;
    }

    if (SendValidSignAndNonces(pClient->pkey, pSession) < 0) {
        cerr << "Error:Impossible send valid sign and nonces" << "\n";
        return FAIL;
    }

    Message message;
    Payload payload;

    message = ReceiveMessage(pSession, NO_CRIPT_MESSAGE, CHECK_SIGN);
    if (message.ct.empty() || message.signature.empty()) {
        std::cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    if (payload.setPeyload(message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(payload.type, CLIENT_VERIFY_TYPE) < 0) {
        cerr << "Error to autenticate oppponent" << "\n";
        return FAIL;
    }

    cout << "it's opponent verify message" << "\n";

    if (payload.my_nonce.empty()) {
        cerr << "Impossible extract Attribute: " << NONCE_ATTRIBUTE << "\n";
        return FAIL;
    }

    if (VerifyNonce(payload.my_nonce, MY_NONCE, pSession) < 0) {
        cerr << "Error to autenticate, nonce is different" << "\n";
        return FAIL;
    }

    if (VerifyNonce(payload.friend_nonce, FRIEND_NONCE, pSession) < 0) {
        cerr << "Error to autenticate, friend nonce is different" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

//SEND PUBKEY AND WAIT SESSION KEY
int OpenConnectWithAddversary(Session *pSession, Client *pClient) {
    cout << "try to connect on address: " << pSession->address.data() << " and port: " << pSession->port << '\n';

    for (int i = 0; i < 5; ++i) { //TRY 3 TIMES TO CONNECT
        pSession->session_id = OpenSession(reinterpret_cast<const char *>(pSession->address.data()), pSession->port);
        if (pSession->session_id > 0) {
            cout << "Connection with adversary is created" << "\n";
            break;
        }
        sleep(10);
    }
    if (pSession->session_id < 0) {
        cerr << "Connection with adversary is failed" << "\n";
        return FAIL;
    }

    if (AutenticateOpponent(pSession, pClient) < 0) {
        cerr << "Error: Impossible autenticate opponent" << '\n';
        return FAIL;
    }

    if (SessionKeyNegotation(pSession, pClient->pkey) < 0) {
        cerr << "Error: Negotation with adversary is failed" << "\n";
        return FAIL;
    }
    return SUCCEED;
}


int SetOpponent(Client *pClient, Session *pSession, Session *pAdversary_session) {
    vector<unsigned char> object;
    Payload payload;

    cout << "wait adversary address.." << '\n';

    Message rec_message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN); //WAIT ADVERSARY RESPONSE
    if (rec_message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }
    rec_message = DecryptMessage(rec_message, pSession->session_key, pSession);
    if (rec_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    if (payload.setPeyload(rec_message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }
    if (payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(payload.type, OPPONENT_ADDRESS_TYPE) > 0) {
        if (payload.address.empty()) {
            cerr << "Error address is empty" << "\n";
            return FAIL;
        }
        //set address
        pAdversary_session->setAddress(payload.address, payload.address.size());
        if (payload.port.empty()) {
            cerr << "Error address is empty" << "\n";
            return FAIL;
        }

        //set port
        int port;
        memcpy(reinterpret_cast<void *>(&port), payload.port.data(), sizeof(int));
        pAdversary_session->port = port;

        if (SendOk(pSession, pClient->pkey, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
            cerr << "Error when try to send ok message" << "\n";
            return FAIL;
        }

        if (ReceiveAndSetPubKeyOfOpponent(pSession, pAdversary_session) < 0) {
            char *err = "Error when try to set adversary certificate";
            cerr << err << "\n";
            if (SendError(pSession, pClient->pkey, err, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
                cerr << "Error when try to send ok message" << "\n";
                return FAIL;
            }
            return FAIL;
        }
        if (SendOk(pSession, pClient->pkey, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
            cerr << "Error when try to send ok message" << "\n";
            return FAIL;
        }
    } else {
        cerr << "Error: type of message is wrong" << "\n";
        return FAIL;
    }
    return SUCCEED;
}

int WaitConnectionWithAdversaryAndConnect(Session *pSession, Client *pClient) {
    //OPEN LISTENER
    if (pClient->OpenListener(pSession->port) < 0) {
        cerr << "Error to open listener" << "\n";
        return FAIL;
    }

    cout << "Open Listener on port: " << pSession->port << '\n';

    cout << "Wait connection by opponent on address: " << &(pClient->addr) << '\n';

    socklen_t addrlen = sizeof(pClient->addr);
    if ((pSession->session_id = accept(pClient->master_socket, (struct sockaddr *) &(pClient->addr),
                                       (socklen_t *) &addrlen)) < 0) {
        perror("accept");
        return FAIL;
    }

    if (AutenticateOpponent(pSession, pClient) < 0) {
        cerr << "Error: Impossible autenticate opponent" << '\n';
        return FAIL;
    }

    if (SessionKeyNegotation(pSession, pClient->pkey) < 0) {
        cerr << "Error:Failed negotation of session key" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

void Application(Client *pClient, Session *pSession) {
    int i, ret;
    unsigned char *user;

    cout << "Choose one of available operations:" << '\n'
         << " 1)List other online users" << '\n'
         << " 2)Check if an other user send me a challenge" << '\n'
         << " 3)Close connection" << '\n';

    do {
        cout << "Choose the operation:";
        cin >> i;
        if (i <= 0 || i > 3) {
            cerr << "\nERROR: You must enter a valid number" << '\n';
            cin.clear();
            // Discard previous input
            cin.ignore(132, '\n');
        } else {
            cin.clear();
            // Discard previous input
            cin.ignore(132, '\n');
            switch (i) {
                case 1:
                    user = ShowUsersList(pClient, pSession); //SHOW AND CHOSE ADVERSARY
                    if (user) {
                        ret = StartChallenge(pClient, pSession, user); //SEND CHALLENGE AND WAIT RESPONSE
                        if (ret < 0) {
                            cerr << "Impossible start the challenge" << '\n';
                        } else if (ret == 0) {
                            cout << "Adversary reject challenge" << '\n';
                        } else {
                            cout << "start connetion " << '\n';
                            Session *pAdversary_session = new Session();
                            if (SetOpponent(pClient, pSession, pAdversary_session) < 0) {
                                cerr << "Error during set adversary cred" << '\n';
                                if (FreeClient(pClient, pSession) < 0) { //warn the server that the challenge is over
                                    cerr << "Error when try to send free client message" << "\n";
                                    break;
                                }
                                break;
                            }
                            if (WaitConnectionWithAdversaryAndConnect(pAdversary_session, pClient) < 0) {
                                cerr << "Error during connection with adversary" << '\n';
                                if (FreeClient(pClient, pSession) < 0) { //warn the server that the challenge is over
                                    cerr << "Error when try to send free client message" << "\n";
                                    break;
                                }
                                break;
                            }
                            cout << "start game " << '\n';
                            if (RunGame(pAdversary_session, pClient, false) < 0) {
                                cerr << "Error during the Match" << '\n';
                            }
                            pClient->CloseListener();
                            close(pAdversary_session->session_id);
                            if (FreeClient(pClient, pSession) < 0) { //warn the server that the challenge is over
                                cerr << "Error when try to send free client message" << "\n";
                                break;
                            }
                            cout << "Match is over" << '\n';

                            break;
                        }
                    }
                    break;
                case 2:
                    ret = SeeChallenge(pClient, pSession);
                    if (ret < 0) {
                        cerr << "Error during see challenge" << '\n';
                    } else if (ret == SUCCEED) {
                        cout << "start connetion " << '\n';
                        Session *pAdversary_session = new Session();
                        if (SetOpponent(pClient, pSession, pAdversary_session) < 0) {
                            cerr << "Error during set adversary cred" << '\n';
                            if (FreeClient(pClient, pSession) < 0) { //warn the server that the challenge is over
                                cerr << "Error when try to send free client message" << "\n";
                                break;
                            }
                            break;
                        }
                        if (OpenConnectWithAddversary(pAdversary_session, pClient) < 0) {
                            cerr << "Error during connection with adversary" << '\n';
                            if (FreeClient(pClient, pSession) < 0) { //warn the server that the challenge is over
                                cerr << "Error when try to send free client message" << "\n";
                                break;
                            }
                            break;
                        }
                        cout << "start game " << '\n';
                        if (RunGame(pAdversary_session, pClient, true) < 0) {
                            cerr << "Error during the Match" << '\n';
                        }
                        close(pAdversary_session->session_id);
                        if (FreeClient(pClient, pSession) < 0) { //warn the server that the challenge is over
                            cerr << "Error when try to send free client message" << "\n";
                            break;
                        }
                        cout << "Match is over" << '\n';

                        break;

                    } else if (ret == 0) {
                        cout << "challenge is rejected " << '\n';
                    } else if (ret == 2) {
                        cout << "No adversary sended a challenge" << '\n';
                    }
                    break;
                case 3:
                    if (CloseConnection(pSession->session_id, pClient, pSession) < 0) {
                        cerr << "ERROR: Impossible close connection" << '\n';
                        i = 4;
                    }
                    cout << "Connection is closed" << '\n';
                    break;
            }
        }

    } while (i != 3);
}

int OpenSession(const char *pHostname, int port) {
    struct sockaddr_in srv_addr;
    int sd, ret;

    memset(&srv_addr, 0, sizeof(srv_addr));
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(port);
    ret = inet_pton(AF_INET, pHostname, &srv_addr.sin_addr);
    if (ret <= 0) {
        cerr << "Invalid server address: " << pHostname << '\n';
        return FAIL;
    }

    /* Creation of new socket */
    sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd == -1) {
        cerr << "Error creating new socket" << '\n';
        return FAIL;
    }

    /* Creating TCP connection */
    ret = connect(sd, (SA *) &srv_addr, sizeof(srv_addr));
    if (ret == -1) {
        cerr << "Error creating the connection" << '\n';
        return FAIL;
    }

    printf("TCP connection with %s established on port %d.\n", pHostname, port);
    return sd;
}
