//
// Created by Federico Cosimo Lapenna on 28/05/2020.
//

#include "Session.h"

using namespace std;

Session::Session() {}

Session::Session(int sessionId) : session_id(sessionId) {}

Session::Session(const Session &obj) {
    if (obj.pPub_key) {
        pPub_key = obj.pPub_key;
    }
    session_id = obj.session_id;
    my_nonce = obj.my_nonce;
    friend_nonce = obj.friend_nonce;
    port = obj.port;
    address = obj.address;
    user_name = obj.user_name;
    challenger_id = obj.challenger_id;

    session_key_t = obj.session_key_t;
    session_key.assign(obj.session_key.begin(), obj.session_key.end());
    session_key.resize(session_key_t);
    user_name_size = obj.user_name_size;
    my_counterGSM = obj.my_counterGSM;
    friend_counterGCM = obj.friend_counterGCM;

}

void Session::setSessionKey(std::vector<unsigned char> session_key, size_t dim) {
    Session::session_key.resize(dim);
    Session::session_key.assign(session_key.begin(), session_key.end());
    session_key_t = dim;
}

void Session::cleanSessionKey() {
    Session::session_key.clear();
}

void Session::setAddress(std::vector<unsigned char> address, size_t dim) {
    Session::address.insert(Session::address.begin(), address.begin(), address.end());
    Session::address.push_back('\0');
    address_t = dim;
}

void Session::setUserName(vector<unsigned char> userName) {
    Session::user_name = new unsigned char[userName.size()];
    memcpy(Session::user_name, userName.data(), userName.size());
}
