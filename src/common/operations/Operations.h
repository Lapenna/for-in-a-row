//
// Created by Federico Cosimo Lapenna on 09/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_OPERATIONS_H
#define CLIENTPROJECTSECURITY_OPERATIONS_H

#include "../model/Session.h"
#include "../model/Message.h"
#include "../../client/model/Client.h"
#include "../model/Payload.h"

int VerifyCertificate(X509_STORE *pStore, X509 *pCert);

int SetSessionNonce(Payload *payload, Session *pSession);

int SendOk(Session *pSession, EVP_PKEY *pKey, int encry_flag, int sign_flag);

int SendError(Session *pSession, EVP_PKEY *pKey, char *err, int encry_flag, int sign_flag);

int CompareVector(std::vector<unsigned char> v, const char *c);

int VerifySing(EVP_PKEY *pPubKey, vector<unsigned char> buf, size_t dim, vector<unsigned char> sign);

int GenNonce();

int SendMessage(int session, vector<unsigned char> v, size_t dim, EVP_PKEY *pKey, int sign_flag);

int WaitResponse(Session *pSession, int encry_flag, int sign_flag);

int ReceiveCertificate(Session *pSession, Client *pClient);

int SendCertificate(Session *pSession, X509 *pCertificate, EVP_PKEY *pKey);

vector<unsigned char> GenerateRandomSequece(size_t dimension);

vector<unsigned char>EncryptMessage(unsigned char *pPt, size_t pt_t, vector<unsigned char> session_key, Session *pSession);

Message DecryptMessage(Message message, vector<unsigned char> session_key, Session *pSession);

Message ReceiveMessage(Session *pSession, int encry_flag, int sign_flag);

int VerifyTypeMessage(vector<unsigned char> message_type, int type);

EVP_PKEY *GetPublicKey(X509 *cert);

vector<unsigned char> getPubKey(EVP_PKEY *key);

vector<unsigned char> GenerateNonceIV(size_t dimension);

int VerifyNonce(vector<unsigned char> nonce, int type, Session *pSession);

int SessionKeyNegotation(Session *pSession, EVP_PKEY *pkey);

#endif //CLIENTPROJECTSECURITY_OPERATIONS_H
