//
// Created by Federico Cosimo Lapenna on 28/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_SERVER_H
#define CLIENTPROJECTSECURITY_SERVER_H


#include <openssl/ossl_typ.h>
#include <cstdio>
#include <map>
#include <list>
#include <netinet/in.h>
#include "../../common/model/Session.h"

using namespace std;

class Server {
public:
    Server();

    Server(const Server &obj);

    int NewSession(int id);

public:
    X509 *certificate;
    EVP_PKEY *pkey;
    std::map<int, Session> clients;
    std::map<unsigned char *, int> clients_to_id;
    std::map<unsigned char *, EVP_PKEY *> clients_key;
    int master_socket;
    struct sockaddr_in addr;

    int RemoveSession(int id);

    int SetChallenge(vector<unsigned char> user_name, int challenger_id);

    int GetUserChallenge(int id);

    int GetIdByName(vector<unsigned char> name);

    int Init(int port);

    int RemoveChallenge(int id);

    void SetNameOnMap(unsigned char *name, int id);

    std::vector<unsigned char> GetAllClients(int user_id);


private:

    void LoadCertificates(const char *pCertFile, const char *pKeyFile);

    int LoadClientsPubKey(map<char *, const char *> clients_key);

    int OpenListener(int port);
};


#endif //CLIENTPROJECTSECURITY_SERVER_H
