//
// Created by Federico Cosimo Lapenna on 28/05/2020.
//

#include <iostream>
#include <openssl/pem.h>
#include <list>
#include <sys/socket.h>
#include <netinet/in.h>
#include "Server.h"
#include "../../common/typeMessage.h"
#include "../../common/operations/Operations.h"

#define BACKLOG_SIZE 10

using namespace std;

Server::Server() {}

int Server::Init(int port) {
    const char *servCert = "Server_cert.pem";
    const char *servKey = "Server_key.pem";

    char *alice_username = "alice";
    const char *alice_pub_key = "AlicePubKey.pem";

    char *bob_username = "bob";
    const char *bob_pub_key = "BobPubKey.pem";

    int opt = 1;
    list<const char *> list;
    map<char *, const char *> client_key;

    client_key[alice_username] = alice_pub_key;
    client_key[bob_username] = bob_pub_key;

    LoadCertificates(servCert, servKey);
    if (LoadClientsPubKey(client_key) < 0) {
        cerr << "Error when try to load client keys" << '\n';
    }

    //create a master socket
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //set master socket to allow multiple connections ,
    //this is just a good habit, it will work without this
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //OPEN LISTENER
    if (OpenListener(port) < 0) {
        std::cerr << "Error to open listener" << "\n";
        abort();
    }

    return 1;
}

int Server::OpenListener(int port) {
    master_socket = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (::bind(master_socket, (struct sockaddr *) &addr, sizeof(addr)) != 0) {
        std::cerr << "can't bind port" << "\n";
        abort();
    }
    if (listen(master_socket, BACKLOG_SIZE) != 0) {
        std::cerr << "Can't configure listening port" << "\n";
        abort();
    }
    return master_socket;
}

int Server::LoadClientsPubKey(map<char *, const char *> clients_key) {
    FILE *pFile;
    EVP_PKEY *pPub_key;

    for (std::map<char *, const char *>::iterator it = clients_key.begin(); it != clients_key.end(); ++it) {
        pFile = fopen(it->second, "r");
        if (!pFile) {
            cerr << "file don't found" << "\n";
            return FAIL;
        }
        pPub_key = PEM_read_PUBKEY(pFile, NULL, NULL, NULL);
        if (!pPub_key) {
            cerr << "error to get pubKey" << "\n";
            return FAIL;
        }
        Server::clients_key[(unsigned char *) it->first] = pPub_key;

        fclose(pFile);
    }

    return SUCCEED;
}

void Server::LoadCertificates(const char *pCertFile, const char *pKeyFile) {
    X509 *pCert;
    EVP_PKEY *pPKey;
    FILE *pFile;

    pFile = fopen(pCertFile, "r");
    if (!pFile) {
        cerr << "Error to open Certificate file" << "\n";
        abort();
    }
    pCert = PEM_read_X509(pFile, NULL, NULL, NULL);
    if (!pCert) {
        cerr << "Certificate error" << "\n";
        abort();
    }
    Server::certificate = pCert;
    fclose(pFile);


    pFile = fopen(pKeyFile, "r");
    if (!pFile) {
        cerr << "Error to open private key file" << "\n";
        abort();
    }
    pPKey = PEM_read_PrivateKey(pFile, NULL, NULL, NULL);
    if (!pPKey) {
        cerr << "Private key error" << "\n";
        abort();
    }
    Server::pkey = pPKey;
    fclose(pFile);
}

int Server::NewSession(int id) {
    if (!clients.count(id)) {
        Session session;
        session.session_id = id;
        clients[id] = session;
        return SUCCEED;
    }
    cerr << "key it's just used" << '\n';
    return FAIL;
}

void Server::SetNameOnMap(unsigned char *name, int id) {
    clients_to_id[name] = id;
}

int Server::RemoveSession(int id) {
    if (clients.count(id)) {
        clients_to_id.erase(clients.at(id).user_name);
        clients.erase(id);
        return SUCCEED;
    }
    cerr << "key it is not used" << '\n';
    return FAIL;
}

vector<unsigned char> Server::GetAllClients(int user_id) {
    vector<unsigned char> users;
    unsigned char *temp;

    for (map<int, Session>::iterator it = clients.begin(); it != clients.end(); ++it) {
        temp = it->second.user_name;
        if (!temp) {
            return vector<unsigned char>();
        }
        if (it->second.session_id != user_id && !it->second.busy) { //IF A USER HAVE A CHALLENGE IS NOT AVAILABLE
            users.insert(users.end(), temp, temp + strnlen((char *) temp, USER_NAME_SIZE_MAX));
            users.push_back('|');
        }
    }
    return users;
}

int Server::SetChallenge(vector<unsigned char> user_name, int challenger_id) {
    for (auto it = clients_to_id.begin(); it != clients_to_id.end(); ++it) {
        if (CompareVector(user_name, (const char *) (it->first)) > 0) {
            int id = it->second;
            if (clients.at(id).challenger_id != 0 || clients.at(id).busy == true) {
                return FAIL;
            }
            clients.at(id).challenger_id = challenger_id;
            return SUCCEED;
        }
    }
    return FAIL;

}

int Server::RemoveChallenge(int id) {
    if (clients.count(id)) {
        for (auto it = clients.begin(); it != clients.end(); ++it) {
            if (it->second.challenger_id == id) {
                it->second.challenger_id = 0;
            }
        }
        clients.at(id).busy = false;
        clients.at(id).challenger_id = 0;
        return SUCCEED;
    }
    return FAIL;
}

int Server::GetIdByName(vector<unsigned char> name) {
    for (std::map<unsigned char *, int>::iterator it = clients_to_id.begin(); it != clients_to_id.end(); ++it) {
        if (CompareVector(name, (const char *) (it->first)) > 0) {
            return it->second;
        }
    }
    return FAIL;
}

int Server::GetUserChallenge(int id) {
    if (clients.count(id)) {
        return clients.at(id).challenger_id;
    }
    return FAIL;
}