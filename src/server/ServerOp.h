//
// Created by Federico Cosimo Lapenna on 09/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_SERVEROP_H
#define CLIENTPROJECTSECURITY_SERVEROP_H

#include "model/Server.h"
#include "../common/model/Session.h"

int Servlet(Server *pServer, Session *pSession);

int WaitResponse(Session *pSession, int encry_flag, int sign_flag);

int ConnectClient(Server *pServer, Session *pSession);

#endif //CLIENTPROJECTSECURITY_SERVEROP_H
