//
// Created by Federico Cosimo Lapenna on 09/05/2020.
//
#include "ServerOp.h"
#include <list>
#include <iostream>
#include "openssl/ssl.h"
#include "../common/operations/Operations.h"
#include "model/Server.h"

//////////////////////////AUTENTICATION PROTOCOL//////////////////////////////////

int AttendClientValidation(Session *pSession) {
    Message message;
    Payload payload;

    message = ReceiveMessage(pSession, NO_CRIPT_MESSAGE, CHECK_SIGN);
    if (message.ct.empty() || message.signature.empty()) {
        std::cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    if (payload.setPeyload(message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(payload.type, CLIENT_VERIFY_TYPE) < 0) {
        std::cerr << "Error to autenticate the client" << "\n";
        return FAIL;
    }

    std::cout << "it's was client verify message" << "\n";

    if (payload.my_nonce.empty()) {
        std::cerr << "Impossible extract Attribute: " << NONCE_ATTRIBUTE << "\n";
        return FAIL;
    }

    if (VerifyNonce(payload.my_nonce, MY_NONCE, pSession) < 0) {
        cerr << "Error to autenticate, my nonce is different" << "\n";
        return FAIL;
    }

    if (VerifyNonce(payload.friend_nonce, FRIEND_NONCE, pSession) < 0) {
        cerr << "Error to autenticate, friend nonce is different" << "\n";
        return FAIL;
    }

    std::cout << "Client verify!" << "\n";

    return SUCCEED;
}

int SendServerHello(Session *pSession, EVP_PKEY *pkey, EVP_PKEY *pPubKey) {
    int nonce;
    vector<unsigned char> message;
    Payload payload;

    nonce = GenNonce(); //GENERATE FRESH NUMBER
    if (nonce == 0) {
        std::cerr << "Error:Invalid nonce " << "\n";
        return FAIL;
    }
    pSession->my_nonce = nonce;

    payload.setType(SERVER_HELLO_TYPE, SEND_MSG);
    payload.setFriendNonce(pSession->friend_nonce, SEND_MSG);
    payload.setMyNonce(nonce, SEND_MSG);

    message = payload.getPayload();
    if (message.empty()) {
        cerr << "Error to get Payload" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, message, message.size(), pkey, CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int AutenticateClient(Session *pSession, Server *pServer) {
    cout << "Start Client autentication" << "\n";

    if (SendCertificate(pSession, pServer->certificate, pServer->pkey) < 0) { //SEND CERTIFICATE
        cerr << "Error:Impossible send certificate" << "\n";
        return FAIL;
    }
    if (WaitResponse(pSession, NO_CRIPT_MESSAGE, NO_CHECK_SIGN) < 0) { //SERVER ATTEND CLIENT OK
        cerr << "Error:Client reject certificate, it's impossible complete the autentication" << "\n";
        return FAIL;
    }
    if (SendServerHello(pSession, pServer->pkey, X509_get_pubkey(pServer->certificate)) < 0) { //SEND SERVER HELLO
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }
    if (AttendClientValidation(pSession) < 0) { //CHECH VALIDATION OF CLIENT
        char *err = "Error: Sign is not valid";
        cerr << err << "\n";
        if (SendError(pSession, pServer->pkey, err, NO_CRIPT_MESSAGE, CHECK_SIGN) < 0) {
            cerr << "Error to send error message to server " << "\n";
            return FAIL;
        }
        cerr << "Error: Client validation error" << "\n";
        return FAIL;
    } else {
        if (SendOk(pSession, pServer->pkey, NO_CRIPT_MESSAGE, CHECK_SIGN) < 0) {
            cerr << "Error to send ok message to server " << "\n";
            return SUCCEED;
        }
    }

    return SUCCEED;
}

int CheckIfClientIsRegisteredAndSetSessionParameters(vector<unsigned char> user_name,
                                                     Server *pServer, Session *pSession) {
    int len;
    char *err = new char[RESPONSE_ATTRIBUTE_DIM];
    for (auto it = pServer->clients_key.begin(); it != pServer->clients_key.end(); ++it) {
        if (CompareVector(user_name, (const char *) (it->first)) > 0) { //CHECK IF THE CLIENT IS REGISTERED
            if (pServer->GetIdByName(user_name) < 0) { //CHECK IF THE CLIENT IS ALREADY ONLINE
                cout << "User is registered" << '\n';
                pSession->setUserName(user_name);
                pSession->user_name_size = user_name.size();
                pSession->pPub_key = it->second;
                pServer->SetNameOnMap(pSession->user_name, pSession->session_id);

                if (SendOk(pSession, pServer->pkey, NO_CRIPT_MESSAGE, NO_CHECK_SIGN) < 0) {
                    cerr << "Error to send ok message to server " << "\n";
                    return FAIL;
                }

                return SUCCEED;
            } else {
                len = strlen("Client id already online");
                strncpy(err, "Client id already online", len);
                err[len] = '\0';
                goto err;
            }

        }
    }
    len = strlen("Client is not registered on server");
    strncpy(err, "Client is not registered on server", len);
    err[len] = '\0';

    err:
    if (SendError(pSession, pServer->pkey, err, NO_CRIPT_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send error message to server " << "\n";
        return FAIL;
    }
    return FAIL;
}

int ConnectClient(Server *pServer, Session *pSession) {
    cout << "Start autentication with client" << "\n";
    Payload payload;

    Message message = ReceiveMessage(pSession, NO_CRIPT_MESSAGE, NO_CHECK_SIGN);
    if (message.ct.empty() || message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        goto err;
    }

    if (payload.setPeyload(message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        goto err;
    }
    if (VerifyTypeMessage(payload.type, CLIENT_HELLO_TYPE) > 0) {
        cout << "Server recived client_hello message" << "\n";

        if (payload.user.empty()) {
            cerr << "Impossible extract Attribute: " << USER_NAME_ATTRIBUTE << "\n";
            return FAIL;
        }
        if (CheckIfClientIsRegisteredAndSetSessionParameters(payload.user, pServer, pSession) < 0) {
            cerr << "Error to autenticate client" << "\n";
            goto err;
        }

        if (SetSessionNonce(&payload, pSession) < 0) { //SET SESSION NONCE
            cerr << "Error to set session nonce" << "\n";
            goto err;
        }

        if (AutenticateClient(pSession, pServer) < 0) {
            cerr << "Error:Failed authentication " << "\n";
            goto err;
        }
        if (SessionKeyNegotation(pSession, pServer->pkey) < 0) {
            cerr << "Error:Failed negotation of session key" << "\n";
            goto err;
        }
    } else {
        cerr << "Type message is wrong" << "\n";
        goto err;
    }

    return SUCCEED;

    err:
    return FAIL;
}

//////////////////////////APPLICATION//////////////////////////////////

int SendAvailableUsers(Server *pServer, Session *pSession) {
    vector<unsigned char> message;
    Payload payload;

    vector<unsigned char> users = pServer->GetAllClients(pSession->session_id);
    payload.setType(AVAILABLE_USERS_TYPE, SEND_MSG);
    payload.setUsers(users);

    message = payload.getPayload();
    message = EncryptMessage(message.data(), message.size(), pSession->session_key, pSession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }
    if (SendMessage(pSession->session_id, message, message.size(), pServer->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int ReceiveChallenge(Payload *payload, Server *pServer, Session *pSession) {
    vector<unsigned char> opponent = payload->user;

    if (opponent.empty()) {
        char *err = "Impossible set challenge, probably user have another challenge";
        cerr << "Impossible extract Attribute: " << NONCE_ATTRIBUTE << "\n";
        SendError(pSession, pServer->pkey, err, GCM_MESSAGE, NO_CHECK_SIGN);
        return FAIL;
    }
    if (pServer->SetChallenge(opponent, pSession->session_id) < 0) {
        char *err = "Impossible set challenge, probably user have another challenge";
        cerr << err << '\n';
        SendError(pSession, pServer->pkey, err, GCM_MESSAGE, NO_CHECK_SIGN);
        return FAIL;
    }
    pSession->busy = true;
    if (SendOk(pSession, pServer->pkey, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error when try to send ok after recive challenge" << "\n";
        return FAIL;
    }
    return SUCCEED;
}

int FreeUser(Server *pServer, Session *pSession) {
    if (pServer->RemoveChallenge(pSession->session_id) < 0) {
        cerr << "Impossible free client,critical error, abort " << "\n";
        abort();
    }
    return SUCCEED;
}

int SendChallenge(Server *pServer, Session *pSession) {
    int challenger_id;
    vector<unsigned char> message;
    Payload payload;

    challenger_id = pServer->GetUserChallenge(pSession->session_id);
    payload.setType(CHALLENGES_RECEIVED_TYPE, SEND_MSG);
    if (challenger_id != 0) {
        Session challenger = pServer->clients.at(challenger_id);
        if (payload.setUserName(challenger.user_name, challenger.user_name_size, SEND_MSG) < 0) {
            cerr << "Error impossible set payload " << "\n";
            return FAIL;
        }
    }
    message = payload.getPayload();
    message = EncryptMessage(message.data(), message.size(), pSession->session_key, pSession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }
    if (SendMessage(pSession->session_id, message, message.size(), pServer->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int RejectRequest(Server *pServer, vector<unsigned char> adversary) {
    int challenger_id;
    vector<unsigned char> message;
    Session *adversarySession;
    Payload payload;

    challenger_id = pServer->GetIdByName(adversary);
    if (challenger_id <= 0) {
        cerr << "User don't exist" << "\n";
        return FAIL;
    } else {
        pServer->clients.at(challenger_id).busy = false; //free the client
        adversarySession = &pServer->clients.at(challenger_id);
    }
    payload.setType(REJECT_TYPE, SEND_MSG);

    message = payload.getPayload();
    message = EncryptMessage(message.data(), message.size(), adversarySession->session_key, adversarySession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(adversarySession->session_id, message, message.size(), pServer->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }
    ///REMOVE PENDING REQUEST
    if (pServer->RemoveChallenge(adversarySession->session_id) < 0) {
        cerr << "Error when try to remove challenge, CRITIC ERROR CLOSE PROGRAM" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int AcceptRequest(Server *pServer, vector<unsigned char> adversary) {
    int challenger_id;
    vector<unsigned char> message;
    Session *adversarySession;
    Payload payload;

    challenger_id = pServer->GetIdByName(adversary);
    if (challenger_id <= 0) {
        cerr << "User don't exist" << "\n";
        return FAIL;
    } else {
        adversarySession = &pServer->clients.at(challenger_id);
    }

    payload.setType(ACCEPT_TYPE, SEND_MSG);
    message = payload.getPayload();
    message = EncryptMessage(message.data(), message.size(), adversarySession->session_key, adversarySession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(adversarySession->session_id, message, message.size(), pServer->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }
    return SUCCEED;
}

int SendCredential(Server *pServer, Session *pSession, Session *pAdversarySession) {
    vector<unsigned char> message;
    Payload payload;


    payload.setType(OPPONENT_ADDRESS_TYPE, SEND_MSG);
    if (payload.setAddress(pAdversarySession->address.data(), pAdversarySession->address_t, SEND_MSG) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }
    payload.setPort(CHALLENGEPORT, SEND_MSG);

    message = payload.getPayload();
    message = EncryptMessage(message.data(), message.size(), pSession->session_key, pSession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    //SEND IP AND PORT
    if (SendMessage(pSession->session_id, message, message.size(), pServer->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    cout << "sent address: " << pAdversarySession->address.data() << " to " << pSession->user_name << '\n';
    message.clear();

    if (WaitResponse(pSession, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "session parameters are not correctly setted by the client" << '\n';
        return FAIL;
    }

    //get pubKey of adversary
    vector<unsigned char> v = getPubKey(pAdversarySession->pPub_key);
    if (v.empty()) {
        cerr << "Error pub key is empty" << "\n";
        return FAIL;
    }

    message = EncryptMessage(v.data(), v.size(), pSession->session_key, pSession);
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    //SEND PUB KEY
    if (SendMessage(pSession->session_id, message, message.size(), pServer->pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send pub key" << "\n";
        return FAIL;
    }

    cout << "sent public key to " << pSession->user_name << '\n';

    if (WaitResponse(pSession, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error to close connection" << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int ActiveGame(Server *pServer, Session *pSession, vector<unsigned char> adversary) {
    int challenger_id;
    vector<unsigned char> message;
    Session *adversarySession;

    challenger_id = pServer->GetIdByName(adversary);
    if (challenger_id == 0) {
        cerr << "User don't exist" << "\n";
        return FAIL;
    } else {
        adversarySession = &pServer->clients.at(challenger_id);
    }

    //SEND TO WHO REQUEST THE CHALLENGE
    if (SendCredential(pServer, adversarySession, pSession) < 0) {
        cerr << "error when try to send credential for peer to peer connection " << "\n";
        goto err;
    }

    //SEND TO WHO ACCEPTED THE REQUEST
    if (SendCredential(pServer, pSession, adversarySession) < 0) {
        cerr << "error when try to send credential for peer to peer connection " << "\n";
        goto err;
    }
    pSession->busy = true;
    pServer->clients.at(challenger_id).busy = true;

    return SUCCEED;

    err:
    pSession->busy = false;
    pServer->clients.at(challenger_id).busy = false;
    return FAIL;

}

int Servlet(Server *pServer, Session *pSession) { // Serve the connection -- threadable
    vector<unsigned char> type;
    vector<unsigned char> object;
    Payload payload;

    Message message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN);
    if (message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return CLOSECONN;
    }
    message = DecryptMessage(message, pSession->session_key, pSession);
    if (message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return 0;
    }

    if (payload.setPeyload(message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }
    type = payload.type;

    if (type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return 0;
    }

    if (VerifyTypeMessage(type, AVAILABLE_USERS_TYPE) > 0) { ///CLIENT ASK AVAILABLE USERS
        if (SendAvailableUsers(pServer, pSession) < 0) {
            cerr << "Error when try to send list of users" << "\n";
            return 0;
        }
        return SUCCEED;
    } else if (VerifyTypeMessage(type, CHALLENGE_USER_TYPE) > 0) { ///CLIENT ASK TO SEND A CHALLENGE
        if (ReceiveChallenge(&payload, pServer, pSession) < 0) {
            cerr << "Error when try to set challenge" << "\n";
            return 0;
        }
        return SUCCEED;

    } else if (VerifyTypeMessage(type, CHALLENGES_RECEIVED_TYPE) > 0) {
        if (SendChallenge(pServer, pSession) < 0) {
            cerr << "Error when try to accept challenge" << "\n";
            return 0;
        }
    } else if (VerifyTypeMessage(type, CLOSE_CONN_TYPE) > 0) {
        if (SendOk(pSession, pServer->pkey, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
            cerr << "Error when try to send ok after close conn" << "\n";
            return FAIL;
        }
        if (pSession->challenger_id != 0) { ///IF THIS CLIENT HAVE A CHALLENGE AWARE THE CHALLENGER
            unsigned char *challengere_user = pServer->clients.at(pSession->challenger_id).user_name;
            object.insert(object.begin(), challengere_user,
                          challengere_user + strnlen((char *) challengere_user, USER_NAME_SIZE_MAX));
            if (RejectRequest(pServer, object) < 0) {
                cerr << "Error when try to reject challenge" << "\n";
                return FAIL;
            }
        }
        return CLOSECONN;
    } else if (VerifyTypeMessage(type, REJECT_TYPE) > 0) {
        object = payload.user;

        if (RejectRequest(pServer, object) < 0) {
            cerr << "Error when try to reject challenge" << "\n";
            return 0;
        }
    } else if (VerifyTypeMessage(type, ACCEPT_TYPE) > 0) {
        object = payload.user;
        if (AcceptRequest(pServer, object) < 0) {
            cerr << "Error when try to accept challenge" << "\n";
            return 0;
        }
        if (ActiveGame(pServer, pSession, object) < 0) {
            cerr << "Error when try to send addresses and ports" << "\n";
            return 0;
        }
    } else if (VerifyTypeMessage(type, FREE_CLIENT_TYPE) > 0) { //FREE CLIENTS
        if (FreeUser(pServer, pSession) < 0) {
            cerr << "Error when try to remove client challenge" << "\n";
            return 0;
        }
    } else {
        char *err = "COMMAND INVALID";
        SendError(pSession, pServer->pkey, err, GCM_MESSAGE, NO_CHECK_SIGN);
        cerr << err << "\n";
        return 0;
    }
    return 0;
}