//
// Created by Federico Cosimo Lapenna on 07/05/2020.
//
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <search.h>
#include <iostream>
#include <unistd.h>
#include <openssl/crypto.h>
#include <openssl/evp.h>
#include "ServerOp.h"
#include "../common/typeMessage.h"

#define PORT '9999'
#define BACKLOG_SIZE 10
#define MAX_CHAR_ADDRESS 15

int main(int argc, char *argv[]) {
    int max_sd, sd, activity, new_socket;
    Server *pServer = new Server();
    //set of socket descriptors
    fd_set readfds;
    vector<unsigned char> address;

    OPENSSL_init();
    OpenSSL_add_all_algorithms();

    max_sd = BACKLOG_SIZE;
    if (pServer->Init(PORT) < 0) {
        std::cerr << "impossible init server" << '\n';
        abort();
    }

    socklen_t len = sizeof(pServer->addr);

    while (1) {
        FD_ZERO(&readfds);

        //add master socket to set
        FD_SET(pServer->master_socket, &readfds);
        max_sd = pServer->master_socket;

        for (std::map<int, Session>::iterator it = pServer->clients.begin(); it != pServer->clients.end(); it++) {
            sd = it->first; //GET SOCKET OF ONLINE CLIENT

            //if valid socket descriptor then add to read list
            if (sd > 0) {
                FD_SET(sd, &readfds);
            }
            //highest file descriptor number, need it for the select function
            if (sd > max_sd) {
                max_sd = sd;
            }
        }

        activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
        if ((activity < 0) && (errno != EINTR)) {
            printf("select error");
        }
        if (FD_ISSET(pServer->master_socket, &readfds)) {
            if ((new_socket = accept(pServer->master_socket, (struct sockaddr *) &(pServer->addr),
                                     (socklen_t *) &len)) < 0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }

            char *addr = inet_ntoa(pServer->addr.sin_addr);

            //inform user of socket number - used in send and receive commands
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n", new_socket,
                   addr, ntohs(pServer->addr.sin_port));

            //ADD ON SOCKET LIST
            if (pServer->NewSession(new_socket) < 0) {
                std::cerr << "CRITIC ERROR: Impossible create new session" << '\n';
                abort();
            }

            //SET ADDRESS AND PORT OF CLIENT
            address.clear();
            address.insert(address.begin(), addr, addr + strnlen(addr, MAX_CHAR_ADDRESS));
            pServer->clients.at(new_socket).setAddress(address, address.size());
            pServer->clients.at(new_socket).port = ntohs(pServer->addr.sin_port);

            if (ConnectClient(pServer, &pServer->clients.at(new_socket)) < 0) { //AUTENTICATE CLIENT
                if (pServer->RemoveSession(new_socket) < 0) { //REMOVE SOCKET IF THE AUTENTICATION IS FAILED
                    std::cerr << "CRITIC ERROR, impossible remove session" << '\n';
                    abort();
                }
                close(new_socket);
                std::cerr << "Autenticaticn failed" << '\n';
            } else {
                std::cout << "Autentication ok" << '\n';
            }
        }
        for (std::map<int, Session>::iterator it = pServer->clients.begin(); it != pServer->clients.end(); it++) {
            new_socket = it->first; //GET SOCKET OF ONLINE CLIENT

            if (FD_ISSET(new_socket, &readfds)) {
                if (Servlet(pServer, &pServer->clients.at(new_socket)) == CLOSECONN) {
                    if (pServer->RemoveSession(new_socket) < 0) { //REMOVE SOCKET IF THE AUTENTICATION IS FAILED
                        std::cerr << "CRITIC ERROR, impossible remove session" << '\n';
                        abort();
                    }
                    close(new_socket);
                    std::cout << "session:" << new_socket << " is closed" << '\n';
                    break;
                }
            }
        }
    }
}


